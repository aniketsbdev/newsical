import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewsTabsModule } from './news-tabs/news-tabs.module'


const routes: Routes = [{
  path: 'news',
  loadChildren: './news-tabs/news-tabs.module#NewsTabsModule',
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
