import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsTabsComponent } from './news-tabs.component';
import { MaterialModule } from '../material.module';
import { NewsTabsRoutingModule } from './news-tabs-routing.module';



@NgModule({
  declarations: [NewsTabsComponent],
  imports: [
    CommonModule,
    NewsTabsRoutingModule,
    MaterialModule
  ]
})
export class NewsTabsModule { }
