import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewsTabsComponent } from './news-tabs.component';

const routes: Routes = [

	{
		path: '',
		component: NewsTabsComponent,
		// children: [
		// 	{
		// 		path: 'header/actions',
		// 		component: ActionComponent
		// 	},
		// 	{
		// 		path: 'hub',
		// 		loadChildren: '../hub/hub.module#HubModule',
		// 		canActivateChild: [HubAuthGaurd]
		// 	},
		// 	{
		// 		path: 'hcp',
		// 		loadChildren: '../hcp/hcp.module#HcpModule',
		// 		canActivateChild: [HcpAuthGuard]
		// 	},
		// 	{
		// 		path: 'profile',
		// 		component: ProfileComponent
		// 	}
		// ]
	},
	
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class NewsTabsRoutingModule {
}
