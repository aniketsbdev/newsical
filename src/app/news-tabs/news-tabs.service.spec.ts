import { TestBed } from '@angular/core/testing';

import { NewsTabsService } from './news-tabs.service';

describe('NewsTabsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NewsTabsService = TestBed.get(NewsTabsService);
    expect(service).toBeTruthy();
  });
});
