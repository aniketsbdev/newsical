import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsTabsComponent } from './news-tabs.component';

describe('NewsTabsComponent', () => {
  let component: NewsTabsComponent;
  let fixture: ComponentFixture<NewsTabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsTabsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
