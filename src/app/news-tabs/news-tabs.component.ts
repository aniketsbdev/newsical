import { Component, OnInit } from '@angular/core';
import OutputJson from '../../assets/stub-json/SampleText.json';

@Component({
  selector: 'app-news-tabs',
  templateUrl: './news-tabs.component.html',
  styleUrls: ['./news-tabs.component.scss']
})
export class NewsTabsComponent implements OnInit {

  title = 'newsical';
  showData: any;
  array = [0, 1, 2, 3, 4]

  constructor() { }

  ngOnInit() {
    // console.log(OutputJson);
    this.showData = OutputJson.data;
  }

}
